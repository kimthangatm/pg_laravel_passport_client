Pg OAuth2 Provider for Laravel passport with Socialite and SocialiteProviders

### 1. Install

```
composer require kimthangatm/pg
```

*You can see*

Socialite https://github.com/laravel/socialite

SocialiteProviders https://github.com/SocialiteProviders/Providers

### 2. Config 

#### Edit config/services.php

Add this config in `config/services.php`

```php
'pg' => [
    'client_id' => 3,
    'client_secret' => 'hl8vHMmOFHAhAFs9BOdJmWstGvD55gveL9kjs2OP',
    'redirect' => 'http://127.0.0.1:8080/callback',  

    //Custom endpoint for sandbox
    'oauth_authorize' => 'http://localhost:8000/oauth/authorize',
    'oauth_token' => 'http://localhost:8000/oauth/token',
    'api_user' => 'http://localhost:8000/api/user'
],
```

#### Edit config/app.php

*For providers*

```php
'providers' => [
    // Remove Socialite if you use before
    // remove 'Laravel\Socialite\SocialiteServiceProvider',
    \SocialiteProviders\Manager\ServiceProvider::class,
];
```

*For alias*

```php
'aliases' => [
    'Socialite' => Laravel\Socialite\Facades\Socialite::class,
];
```

#### For router

*Add route in routers/web.php*

```php
Route::get('/login/loginWithPG', 'Auth\LoginController@redirectToProvider')->name('redirectToPG');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback')->name('pgLogin');
```

#### Edit LoginController.php


Edit `app/Http/Controllers/Auth/LoginController.php`. If you want to make fast with basic laravel you can use

```
//Make Larave default auth
php artisan make:auth

//Config database before you migrate
php artisan migrate
```

*So, LoginController.php like*

```php
<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToPGProvider()
    {
        return Socialite::driver('pg')->redirect();
    }

    /**
     * Obtain the user information from PG.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        try{
            $user = Socialite::driver('pg')->user();
            $pg_user_id = $user->getId();
            $email = $user->getEmail();

            //$localUser = User::orWhere('pg_user_id', $pg_user_id)->orWhere('email', $pg_user_id)->first();
            $localUser = User::where('email', $email)->first();
            if (!$localUser) {
                $localUser = new User([
                    'name' => $user->getName(),
                    'email' => $email,
                    'password' => ''
                    //Add pg_user_id if you need
                    //'pg_user_id' => $pg_user_id
                ]);
                $localUser->save();
            }
            Auth::loginUsingId($localUser->id);
            return redirect('/home');
        }catch(\Exception $e){
            //Redirect with you message
            return redirect('/home');
        }
    }
}
```

#### And don't forget edit `app/Providers/EventServiceProvider.php`

So that file look like bellow:

```php
<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\Pg\PgExtendSocialite@handle',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
```