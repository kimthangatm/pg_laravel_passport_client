<?php

namespace SocialiteProviders\Pg;

use SocialiteProviders\Manager\SocialiteWasCalled;

class PgExtendSocialite
{
    /**
     * Execute the provider.
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('pg', __NAMESPACE__.'\Provider');
    }
}
