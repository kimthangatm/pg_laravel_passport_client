<?php

namespace SocialiteProviders\Pg;

use Laravel\Socialite\Two\ProviderInterface;
use SocialiteProviders\Manager\OAuth2\AbstractProvider;
use SocialiteProviders\Manager\OAuth2\User;

class Provider extends AbstractProvider implements ProviderInterface
{
    /**
     * Unique Provider Identifier.
     */
    const IDENTIFIER = 'PG';

    /**
     * {@inheritdoc}
     */
    protected $scopes = [''];

    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase(config('services.pg.oauth_authorize') ? config('services.pg.oauth_authorize') : 'https://payment.ongate.vn/oauth/authorize', $state);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return config('services.pg.oauth_token') ? config('services.pg.oauth_token') : 'https://payment.ongate.vn/oauth/token';
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken($token)
    {
        $apiUser = config('services.pg.api_user') ? config('services.pg.api_user') : 'http://localhost:8000/api/user';

        $response = $this->getHttpClient()->get($apiUser, [
            'headers' => [
                'Authorization' => 'Bearer '.$token,
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        return (new User())->setRaw($user)->map([
            'id'       => $user['id'],
            'nickname' => isset($user['username']) ? $user['username'] : $this->makeUsernameFromEmail($user['email']),
            'name'     => isset($user['name']) ? $user['name'] : $this->makeUsernameFromEmail($user['email']),
            'email'    => $user['email'],
            'avatar'   => isset($user['avatar']) ? $user['avatar'] : null
        ]);
    }

    protected function makeUsernameFromEmail($email)
    {
        $parts = explode('@', $email);
        return $parts[0];
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenFields($code)
    {
        return array_merge(parent::getTokenFields($code), [
            'grant_type' => 'authorization_code'
        ]);
    }
}